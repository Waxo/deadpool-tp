# Unicorns and stuff

## Install
```sh
git clone <this-repository>
yarn
yarn run start
```

# .env
This project uses dotenv if you don't create a .env file this will not work
  
Example of `.env` file: 
```sh
TICK_ENERGY=1
THRESHOLD_TRANSFORM=100
TICK_INTERVAL_ENERGY=1000
RIDING_TIME=1000
```

# Linting
This project uses xo linting, run `xo` to see if anything is ok

# Notes
The code is currently running 10 minutes then stops, please have a look in `index-unicorns-and-stuff.js` if you want to change it.  
There is still a lot of hardcode in this project. It may need more constants in .env
