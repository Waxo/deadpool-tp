const BluebirdPromise = require('bluebird');
const chalk = require('chalk');
const {Spiderman} = require('./spiderman');

let instance = null;

class Deadpool {
  constructor(evClock) {
    if (!instance) {
      instance = this;
      this.ponyList = [];
      this.hp = 100;
      this.ev = evClock;
      this.isDay = false;
      const spiderman = new Spiderman(evClock);
      spiderman.becomeFriend(this);

      this.init_();
    }
    return instance;
  }

  init_() {
    this.startListeners_();
    this.startIntervals_();
  }

  startListeners_() {
    this.ev.on('cycle change', period => {
      switch (period) {
        case 'day':
          this.isDay = true;
          break;
        case 'night':
          this.isDay = false;
          break;
        case 'world end':
          this.stopIntervals_();
          break;
        default:
          break;
      }
    });
  }

  startIntervals_() {
    this.intervalRegen = setInterval(() => {
      const rand = Math.random();
      if ((this.hp < 900 && rand > 0.9) ||
        (this.hp < 750 && rand > 0.75) ||
        (this.hp < 500 && rand > 0.50) ||
        (this.hp < 250 && rand > 0.25) ||
        (this.hp < 100 && rand > 0.10)) {
        for (let i = 0; i < Math.floor(rand * 100); i++) {
          this.regenerate_();
        }
      }

      this.regenerate_();
      this.hp -= Math.floor(Math.random() * 100) * ((this.isDay) ? 1 : 10);
    }, 1000);

    this.intervalState = setInterval(() => {
      this.showState_();
    }, 1000);

    this.intervalGoodbye = setInterval(() => {
      this.ponyList.forEach((pony, index) => {
        if (pony.sayGoodbye) {
          this.ponyList.splice(index, 1);
        }
      });
    }, 10000);
  }

  stopIntervals_() {
    clearInterval(this.intervalRegen);
    clearInterval(this.intervalState);
  }

  showState_() {
    const deadpool = `Deadpool ${this.hp} hp`;
    const listUnicorns = this.ponyList.filter(p => p.isUnicorn);
    const unicorns = `${listUnicorns.length} unicorns`;
    const sleeping = `${this.ponyList.filter(
      p => p.isSleeping).length}/${this.ponyList.length} sleeping`;
    let gatherableEnergy = '';
    if (listUnicorns.length > 0) {
      gatherableEnergy =
        `${listUnicorns.map(u => u.energy).reduce((a, b) => a + b)} energy`;
    }
    console.log(
      `${chalk.red(deadpool)}\t${chalk.blue(unicorns)}\t${chalk.yellow(
        gatherableEnergy)}\t${chalk.cyan(sleeping)}`);
  }

  askToTransform() {
    return new BluebirdPromise((resolve, reject) => {
      setTimeout(() => {
        const nbUnicorns = this.ponyList.filter(p => p.isUnicorn).length;
        if (Math.random() > nbUnicorns / this.ponyList.length) {
          resolve();
        } else {
          reject();
        }
      }, Math.random() * 1000);
    });
  }

  regenerate_() {
    const unicornsList = this.ponyList.filter(p => p.isUnicorn);
    if (unicornsList.length > 0) {
      unicornsList[Deadpool.getOneFormList_(
        unicornsList.length)].regenDeadpool()
        .then(energy => {
          this.hp += energy;
        })
        .catch(() => {});
    }
  }

  static getOneFormList_(length) {
    const l = String(length);
    let tenPow = '1';
    while (tenPow.length <= l.length) {
      tenPow += '0';
    }
    return Math.floor(Math.random() * Number(tenPow)) % length;
  }

  registerPony(pony) {
    this.ponyList.push(pony);
  }

  rideThisOne() {
    return new BluebirdPromise(resolve => {
      setTimeout(() => resolve(
        this.ponyList[Deadpool.getOneFormList_(this.ponyList.length)]), 1000);
    });
  }
}

module.exports = {Deadpool};
