let instance = null;

class Spiderman {
  constructor(evClock) {
    if (!instance) {
      instance = this;
      this.friend = null;
      this.isRiding = false;

      evClock.on('cycle change', ev => {
        if (ev === 'world end') {
          this.greetFriend = () => {};
        }
      });
    }
    return instance;
  }

  greetFriend() {
    setTimeout(() => {
      if (this.isRiding) {
        this.greetFriend();
      } else {
        this.friend.rideThisOne()
          .then(pony => {
            this.isRiding = true;
            return pony.rideMe();
          })
          .then(() => {
            this.isRiding = false;
            this.greetFriend();
          });
      }
    }, Math.random() * 1000);
  }

  becomeFriend(friend) {
    this.friend = friend;
    this.greetFriend();
  }
}

module.exports = {Spiderman};
