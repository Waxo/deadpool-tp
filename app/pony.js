const BluebirdPromise = require('bluebird');
const {Deadpool} = require('./deadpool');

class Pony {
  constructor(evClock) {
    this.ev = evClock;

    this.deadpool = new Deadpool(evClock);
    this.deadpool.registerPony(this);

    this.energy = 0;
    this.isSleeping = false;
    this.isDay = false;
    this.isCurrentlyRided = false;
    this.intervalEnergy = null;
    this.intervalNewPoney = null;
    this.isUnicorn = false;
    this.sayGoodbye = false;

    this.init_();
  }

  init_() {
    this.startListeners_();
    this.startIntervals_();
  }

  startListeners_() {
    this.ev.on('cycle change', period => {
      switch (period) {
        case 'day':
          this.isDay = true;
          break;
        case 'night':
          this.isDay = false;
          break;
        case 'world end':
          this.stopIntervals_();
          break;
        default:
          break;
      }
    });
  }

  startIntervals_() {
    this.intervalEnergy = setInterval(() => {
      if (this.isSleeping) {
        this.energy += Number(process.env.TICK_ENERGY) * 5;
      } else if (this.isCurrentlyRided) {
        this.energy -=
          Number(process.env.TICK_ENERGY) * (this.isDay) ? 10 : 1000;
      } else {
        this.energy += Number(process.env.TICK_ENERGY);
      }

      if (this.energy < 0) {
        this.sayGoodbye = true;
        this.stopIntervals_();
      }
      this.switchSleepState_();
      this.transformToUnicorn_();
    }, Number(process.env.TICK_INTERVAL_ENERGY));

    this.intervalNewPoney = setInterval(() => {
      if (this.isSleeping && Math.random() > 0.99) {
        // eslint-disable-next-line no-new
        new Pony(this.ev);
        this.energy = 0;
      }
    }, 10000);
  }

  stopIntervals_() {
    clearInterval(this.intervalEnergy);
    clearInterval(this.intervalNewPoney);
  }

  switchSleepState_() {
    if (this.isSleeping) {
      this.wakeUp_();
    } else {
      this.fallAsleep_();
    }
  }

  fallAsleep_() {
    if (this.isDay || this.isCurrentlyRided) {
      return;
    }
    this.isSleeping = Math.random() < 0.75;
  }

  wakeUp_() {
    this.isSleeping =
      (this.isDay) ? (Math.random() > 0.5) : (Math.random() < 0.99);
  }

  transformToUnicorn_() {
    if (!this.isUnicorn && !this.isSleeping && this.isDay && this.energy >
      Number(process.env.THRESHOLD_TRANSFORM && Math.random() > 0.75)) {
      this.deadpool.askToTransform()
        .then(() => this.tryTransform_())
        .catch(() => {})
        .finally(() => {
          this.energy = 0;
        });
    }
  }

  tryTransform_() {
    if (Math.random() > 1 - (this.energy / 100)) {
      this.isUnicorn = true;
    }
    this.energy = 0;
  }

  regenDeadpool() {
    return new BluebirdPromise((resolve, reject) => {
      setTimeout(() => {
        if (this.isCurrentlyRided || !this.isUnicorn) {
          reject();
        } else if (Math.random() > 1 - (this.energy * 0.02)) {
          const tmpEnergy = this.energy;
          this.energy = 0;
          this.isUnicorn = false;
          resolve(tmpEnergy);
        } else {
          reject();
        }
      }, Math.random() * 1000);
    });
  }

  rideMe() {
    this.isSleeping = false;
    this.isCurrentlyRided = true;
    return new BluebirdPromise(resolve => {
      setTimeout(() => {
        this.isUnicorn = false;
        this.isCurrentlyRided = false;
        resolve();
      }, Math.random() * Number(process.env.RIDING_TIME));
    });
  }
}

module.exports = {Pony};
