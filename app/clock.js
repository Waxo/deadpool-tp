const {EventEmitter} = require('events');
const chalk = require('chalk');

const clockEvents = new EventEmitter();

let instance = null;

class Clock {
  constructor() {
    if (!instance) {
      instance = this;
      this.interval = null;
      this.time = 6;
    }
    return instance;
  }

  startClock() {
    this.interval = setInterval(() => {
      this.time = (++this.time) % 24;
      if (this.time === 7) {
        console.log(chalk.magenta('day'));
        clockEvents.emit('cycle change', 'day');
      } else if (this.time === 20) {
        console.log(chalk.magenta('night'));
        clockEvents.emit('cycle change', 'night');
      }
    }, 5000);
  }

  stopClock() {
    clockEvents.emit('cycle change', 'world end');
    console.log(chalk.magenta('world end'));
    clearInterval(this.interval);
  }
}

module.exports = {Clock, clockEvents};
