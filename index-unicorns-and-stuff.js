require('dotenv').config();
const {Clock, clockEvents} = require('./app/clock');
const {Pony} = require('./app/pony');

const ponyList = [];

clockEvents.setMaxListeners(10000);

const clock = new Clock();
clock.startClock();

for (let i = 0; i < 1000; i++) {
  ponyList.push(new Pony(clockEvents));
}

setTimeout(() => clock.stopClock(), 1000 * 600);
